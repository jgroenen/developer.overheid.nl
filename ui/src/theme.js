export default {
  color: {
    primary: {
      normal: '#2961ff',
    },
    text: {
      normal: '#2d3240',
      light: '#676d80',
      inactive: '#a3a3a3',
    },
    error: '#e02424',
    notify: '#ab6262',
    tagBackground: '#f1f5ff',
  },
  font: {
    size: {
      tiny: '12px',
      small: '14px',
      normal: '16px',
      title: {
        normal: '20px',
        large: '28px',
      },
    },
    lineHeight: {
      tiny: '20px',
      small: '22px',
      normal: '24px',
      title: {
        normal: '28px',
        large: '36px',
      },
    },
    weight: {
      normal: '400',
      semibold: '600',
      bold: '700',
    },
  },
}
