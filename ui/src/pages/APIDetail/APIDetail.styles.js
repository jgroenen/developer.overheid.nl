import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  max-width: 1240px;
  margin: 0 auto;
  flex-wrap: wrap;
`
