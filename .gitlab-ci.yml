image: docker:stable

stages:
  - validate
  - test
  - release
  - review
  - deploy
  - e2e-tests

variables:
  IMAGE_PREFIX: $CI_REGISTRY_IMAGE
  DOMAIN: developer.overheid.nl
  KUBECTL_CONTEXT: delta.k8s.nlx.io
  REVIEW_BASE_DOMAIN: nlx.reviews
  REVIEW_NAMESPACE: "don-$CI_ENVIRONMENT_SLUG"

.validate:
  image: golang:1.13.5
  before_script:
    - cd validate
  tags:
    - docker
    - linux

Test validate:
  extends: .validate
  stage: test
  script:
    - go test ./... -coverprofile coverage.out
    - go tool cover -html=coverage.out -o coverage.html
    - go tool cover -func=coverage.out
  coverage: /total:\t+\(statements\)\t+([\d\.]+?%)/
  artifacts:
    expire_in: 1 month
    paths:
      - validate/coverage.html

Validate API definitions:
  extends: .validate
  stage: validate
  script:
    - go run ./cmd/don-validate/main.go

## UI ##
.ui:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export UI_IMAGE=${IMAGE_PREFIX}/ui
  tags:
    - docker
    - linux
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind

Test and release UI:
  extends: .ui
  stage: release
  script:
    - |
        docker build \
        --tag $UI_IMAGE:$CI_COMMIT_SHORT_SHA \
        --tag $UI_IMAGE:$CI_COMMIT_REF_SLUG \
        -f ui/Dockerfile . && \
        docker push $UI_IMAGE:$CI_COMMIT_SHORT_SHA && \
        docker push $UI_IMAGE:$CI_COMMIT_REF_SLUG
  coverage: /All\sfiles.*?\s+(\d+.\d+)/

## API ##
.api:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export API_IMAGE=${IMAGE_PREFIX}/api
  tags:
    - docker
    - linux
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind

Test and release API:
  extends: .api
  stage: release
  script:
    - |
        docker build \
        --tag $API_IMAGE:$CI_COMMIT_SHORT_SHA \
        --tag $API_IMAGE:$CI_COMMIT_REF_SLUG \
        -f api/Dockerfile . && \
        docker push $API_IMAGE:$CI_COMMIT_SHORT_SHA && \
        docker push $API_IMAGE:$CI_COMMIT_REF_SLUG
  coverage: /total:\t+\(statements\)\t+([\d\.]+?%)/

## Docs ##
.docs:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export DOCS_IMAGE=${IMAGE_PREFIX}/docs
  tags:
    - docker
    - linux
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind

Release Docs:
  extends: .docs
  stage: release
  script:
    - |
        docker build \
        --tag $DOCS_IMAGE:$CI_COMMIT_SHORT_SHA \
        --tag $DOCS_IMAGE:$CI_COMMIT_REF_SLUG \
        -f docs/Dockerfile . && \
        docker push $DOCS_IMAGE:$CI_COMMIT_SHORT_SHA && \
        docker push $DOCS_IMAGE:$CI_COMMIT_REF_SLUG

## Review App ##
Deploy Review App:
  stage: review
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  script:
    - echo -e -n "https://don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN" > ci_environment_url.txt
    - |
        helm upgrade --install $REVIEW_NAMESPACE ./helm/don \
        --namespace $REVIEW_NAMESPACE \
        --set-string "gitlabProjectId=$GITLAB_PROJECT_ID" \
        --set "gitlabUrl=$GITLAB_URL" \
        --set "gitlabAccessToken=$GITLAB_ACCESS_TOKEN" \
        --set "loadDatabaseFixtures=true" \
        --set "apiImage=$IMAGE_PREFIX/api:$CI_COMMIT_SHORT_SHA" \
        --set "uiImage=$IMAGE_PREFIX/ui:$CI_COMMIT_SHORT_SHA" \
        --set "docsImage=$IMAGE_PREFIX/docs:$CI_COMMIT_SHORT_SHA" \
        --set "domain=don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN" \
        --set "replicas=1" \
        --set "ingressClass=traefik"
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN
    on_stop: Remove Review App
  tags:
    - review
    - kubernetes
  except:
    - master
  artifacts:
    paths:
      - ci_environment_url.txt

Remove Review App:
  stage: review
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    GIT_STRATEGY: none
  script:
    - helm delete --purge $REVIEW_NAMESPACE
    - kubectl delete namespace $REVIEW_NAMESPACE
  when: manual
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN
    action: stop
  tags:
    - review
    - kubernetes
  except:
    - master

## E2E Tests
E2E tests:
  stage: e2e-tests
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  before_script:
    - export URL=$([ -f ci_environment_url.txt ] && cat ci_environment_url.txt || echo "https://$DOMAIN")
    - export E2E_IMAGE=${IMAGE_PREFIX}/e2e
  script:
    - |
      docker build --tag $E2E_IMAGE:$CI_COMMIT_SHORT_SHA \
      -f e2e-tests/Dockerfile . && \
      docker run --rm --cap-add=SYS_ADMIN \
      -e URL=${URL} \
      $E2E_IMAGE:$CI_COMMIT_SHORT_SHA \
      /bin/sh -c "./wait-for-http.sh $URL && npm test"
  tags:
    - docker
    - linux
  services:
    - docker:dind

## Depoyment ##
Deploy production:
  stage: deploy
  before_script:
    - kubectl config use-context $KUBECTL_CONTEXT
  script:
    - |
        helm upgrade --install don-prod ./helm/don \
        --namespace don-prod \
        --set-string "gitlabProjectId=$GITLAB_PROJECT_ID" \
        --set "gitlabUrl=$GITLAB_URL" \
        --set "gitlabAccessToken=$GITLAB_ACCESS_TOKEN" \
        --set "apiImage=$IMAGE_PREFIX/api:$CI_COMMIT_SHORT_SHA" \
        --set "uiImage=$IMAGE_PREFIX/ui:$CI_COMMIT_SHORT_SHA" \
        --set "docsImage=$IMAGE_PREFIX/docs:$CI_COMMIT_SHORT_SHA" \
        --set domain="$DOMAIN" \
        --set "ingressClass=traefik"
  environment:
    name: prod
    url: https://$DOMAIN
  tags:
    - nlx-privileged
    - shell
  only:
    - master
